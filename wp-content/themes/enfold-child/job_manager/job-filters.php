<?php
/**
 * Filters in `[jobs]` shortcode.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/job-filters.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager
 * @category    Template
 * @version     1.33.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

wp_enqueue_script( 'wp-job-manager-ajax-filters' );

do_action( 'job_manager_job_filters_before', $atts );
?>

<form class="job_filters">
	<?php do_action( 'job_manager_job_filters_start', $atts ); ?>

	<div class="search_jobs">
		<?php do_action( 'job_manager_job_filters_search_jobs_start', $atts ); ?>

		<div class="search_keywords_col flex_column av_four_fifth flex_column_div av-zero-column-padding first  avia-builder-el-1  el_after_av_textblock  el_before_av_one_fift">
			<label for="search_keywords"><?php esc_html_e( 'Search for...', 'wp-job-manager' ); ?></label>
			<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'Search for...', 'wp-job-manager' ); ?>" value="<?php echo esc_attr( $keywords ); ?>" />
		</div>
        <div class="search_keywords_col flex_column av_one_fifth flex_column_div av-zero-column-padding   avia-builder-el-2  el_after_av_four_fifth  avia-builder-el-last">
            <?php
            global $wpdb;
            $job_locations = $wpdb->get_results( 'SELECT DISTINCT meta_value FROM wp_postmeta WHERE meta_key LIKE "_job_location"', ARRAY_A );
            if ( is_array( $job_locations ) ):
                ?>
                <select name="search_location" id="search_location">
                    <option  value="" <?php selected( empty( $location ) || ! in_array( $location, $job_locations ) ); ?>><span style="color: #a8acad !important;"><?php _e( 'Location' ); ?></span></option>
                    <?php foreach( $job_locations as $loc_val ): ?>
                        <option value="<?php echo esc_attr( $loc_val['meta_value'] ); ?>" <?php selected( $location, $loc_val['meta_value'] ); ?>><?php echo esc_attr( $loc_val['meta_value'] ); ?></option>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>
            <?php
            /**
             * Show the submit button on the job filters form.
             *
             * @since 1.33.0
             *
             * @param bool $show_submit_button Whether to show the button. Defaults to true.
             * @return bool
             */
            if ( apply_filters( 'job_manager_job_filters_show_submit_button', true ) ) :
                ?>
                <div class="job_search_submit">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </div>
            <?php endif; ?>
        </div>
		<?php do_action( 'job_manager_job_filters_search_jobs_end', $atts ); ?>
	</div>

	<?php do_action( 'job_manager_job_filters_end', $atts ); ?>
</form>

<?php do_action( 'job_manager_job_filters_after', $atts ); ?>

<noscript><?php esc_html_e( 'Your browser does not support JavaScript, or it is disabled. JavaScript must be enabled in order to view listings.', 'wp-job-manager' ); ?></noscript>
