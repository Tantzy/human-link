<?php
/**
 * Job listing in the loop.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/content-job_listing.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager
 * @category    Template
 * @since       1.0.0
 * @version     1.27.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;
?>
<li <?php job_listing_class(); ?> data-longitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_long ); ?>">
	<div class="job-details flex_column av_four_fifth flex_column_div av-zero-column-padding first  avia-builder-el-1  el_after_av_textblock  el_before_av_one_fift">
        <div class="position">
            <h3><?php wpjm_the_job_title(); ?></h3>
        </div>
        <div class="location">
            <?php the_job_location( false ); ?>
        </div>
    </div>
    <div class="job-direct flex_column av_one_fifth flex_column_div av-zero-column-padding   avia-builder-el-2  el_after_av_four_fifth  avia-builder-el-last">
        <a href="<?php the_job_permalink(); ?>" class="avia-button   avia-icon_select-yes-left-icon avia-color-theme-color avia-size-small avia-position-right ">
            <span class="avia_iconbox_title">APPLY</span>
        </a>
    </div>

</li>
