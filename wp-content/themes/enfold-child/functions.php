<?php


function social_bookmarks_register_widget() {
register_widget( 'add_social_bookmarks' );
}
add_action( 'widgets_init', 'social_bookmarks_register_widget' );

class add_social_bookmarks extends WP_Widget {
public function __construct() {
  $widget_ops = array(
    'classname' => 'socialbookmarks-widget',
    'description' => __('A widget that displays the social bookmarks', 'avia_framework')
    );
  parent::__construct( 'add_social_bookmarks', THEMENAME.' Social Bookmarks', $widget_ops );
}

public function widget( $args, $instance ) {
  $title = apply_filters( 'widget_title', $instance['title'] );
  echo $args['before_widget'];
  //if title is present
  if ( ! empty( $title ) )
  echo $args['before_title'] . $title . $args['after_title'];
  //output
  echo $before_widget;
  $social_args = array('outside'=>'ul', 'inside'=>'li', 'append' => '');
  echo avia_social_media_icons($social_args, false);
  echo $after_widget;
}

public function form( $instance ) {
  if ( isset( $instance[ 'title' ] ) )
  $title = $instance[ 'title' ];
  else
  $title = __( 'Social Bookmarks', 'avia_framework' );
  ?>
  <p>
  <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
  </p>
  <?php
}

public function update( $new_instance, $old_instance ) {
  $instance = array();
  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
  return $instance;
}
}


// Register Socket Menu

add_action( 'init', 'register_my_new_menu' );
function register_my_new_menu() {
  register_nav_menu('footer-second',__( 'Footer Second Menu' ));
}


function get_location_values( $meta_key='_job_location',  $post_type = 'job_listing' ) {

    $posts = get_posts(
        array(
            'post_type' => $post_type,
            'meta_key' => $meta_key,
            'posts_per_page' => -1,
        )
    );

    $meta_values = array();
    foreach( $posts as $post ) {
        $meta_values[] = get_post_meta( $post->ID, $meta_key, true );
    }

    return $meta_values;

}

$meta_values = get_location_values( $meta_key, $post_type );





/**
 * This code gets your posted field and modifies the job search query
 */
add_filter( 'job_manager_get_listings', 'filter_by_salary_field_query_args', 10, 2 );
function filter_by_salary_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by salary
		if ( ! empty( $form_data['filter_by_salary'] ) ) {
			$selected_range = sanitize_text_field( $form_data['filter_by_salary'] );
			 $posts = get_posts(
                                array(
                                    'post_type' => 'job_listing',
                                    'meta_key' => '_job_location',
                                    'posts_per_page' => -1,
                                )
                            );

                            $meta_values = array();
                            foreach ($meta_values as &$value) {
                                echo $value;
                            }
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	return $query_args;
}



add_action( 'wp_enqueue_scripts', 'add_custom_fa_css' );

function add_custom_fa_css() {
    wp_enqueue_style( 'custom-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
}

